#!/bin/env zsh

# Link everything in the home directory
ln -st ~ .
ln -st ~ .zsh.d
ln -st ~ .fonts.d 
ln -st ~ .antigen.d 
ln -st ~ .p10k.d 
ln -st ~ ~/.p10k.d/.p10k.zsh
ln -st ~ ~/.zsh.d/.zshenv 
ln -st ~ ~/.zsh.d/.zshrc 
ln -st ~ ~/.antigen.d/.antigenrc

# This needs literal linking
ln -s ~/.fonts.d ~/.local/share/fonts
