typeset -UgxT PATH path
typeset -UgxT FPATH fpath
typeset -UgxT INFOPATH infopath
typeset -UgxT MANPATH manpath

setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS

# Set lang etc
typeset -xg LC_ALL=en_GB.UTF-8
typeset -xg LANG=en_GB.UTF-8
typeset -xg LANGUAGE=en_GB:en
typeset -xg HISTFILE=${ZDOTDIR}/.zsh_history
typeset -xg GIT_DISCOVERY_ACROSS_FILESYSTEM=false
typeset -xg GIT_CEILING_DIRECTORIES="/home"

# Ensure we can use local binaries, but not before system ones
path+=("${HOME}/.local/bin")

# Add local completions
fpath+=("${HOME}/.zsh.d/site-functions/")

# Put the stuff we put in /opt ourselves before the rest.
path=("/opt/bin/" "/opt/sbin" $path)
manpath=("/opt/share/man/" $manpath)
infopath=("/opt/share/info/" $infopath)

# Set all the brew variables
if type /home/linuxbrew/.linuxbrew/bin/brew &> /dev/null; then
	
	typeset -xg HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew";
	typeset -xg HOMEBREW_CELLAR="${HOMEBREW_PREFIX}/Cellar";
	typeset -xg HOMEBREW_REPOSITORY="${HOMEBREW_PREFIX}/Homebrew";
	
	# Make sure we can get to brew (but allow local binaries first)
	path+=("${HOMEBREW_PREFIX}/bin" "${HOMEBREW_PREFIX}/sbin")
	manpath+=("${HOMEBREW_PREFIX}/share/man")
	infopath+=(${HOMEBREW_PREFIX}/share/info)

	typeset -xg HOMEBREW_NO_ENV_HINTS=1
fi
